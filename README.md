# googleadmob Service
===================================

![googleadmob-popover](images/popover.png)

### Description

# Documentación

This service allows you to import a WordPress menu into the mobile application.

To use this service, activate it and in the ** Modules ** view a button will appear with the name 'Import menu'.

The configuration view will then open, which is divided into 4 steps.

1. Instructions and requirements to follow.
2. Enter the url of your WordPress home page to search for it
3. Select which WordPress menu you want to import
4. Select what type of menus you want in your mobile app

### Requisitos
It will be necessary to have the plugin [**WP API Menus**](https://wordpress.org/plugins/wp-api-menus/) installed on your WordPress page
### Images
- [Logo](images/logo.png)
- [Screenshot](images/screenshot01.png)


### Details:

- Author:
- Version: 0.0.1
- Homepage:
