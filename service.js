(function () {
  angular
    .module('king.services.wpmenuimport', [])
    .run(wpmenuimport);

  wpmenuimport.$inject = ['configService'];

  function wpmenuimport(configService) {
    console.log("Wordpress menu service is active");
  }
})();
