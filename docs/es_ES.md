# Documentación

Este servicio le permite importar un menú de WordPress a la aplicación móvil.

Para usar este servicio, actívelo y en la vista de **Modulos** aparecerá un botón con el nombre 'Importar menú'.

A continuación se abrirá la vista de configuración, la cual se divide en 4 pasos.

1. Instrucciones y requisitos a seguir.
2. Introduce la url de la pagina principal de tu WordPress para buscarlo
3. Selecciona que menú del WordPress deseas importar
4. Selecciona que tipo de menús quieres en tu app de móvil

### Requisitos
Será necesario tener instalado el plugin [**WP API Menus**](https://wordpress.org/plugins/wp-api-menus/) en la pagina WordPress